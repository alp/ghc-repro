Run a given CI job (`release-x86_64-linux-deb9-dwarf`,
`validate-x86_64-linux-deb9-hadrian`, ...), but locally, using Docker.

This program generates a pair of scripts, `docker.sh` and `build.sh`.

- `build.sh` sets all the environment variables right and contains the concatenated
build script for the given job. When executed, it effectively runs the whole
build process that the given CI job involves.

- `docker.sh` has a few Docker commands to fire up a container, copy `build.sh`
   there and run it.


You'll want to run the latter.

# Usage

```
ghc-repro - run CI jobs locally with Docker

Usage: ghc-repro [-f|--file FILE] (-j|--job JOBNAME) [-u|--user USER]
                 [-p|--project REPONAME] [-r|--ref GIT_REF]
  Automate the execution of GHC jobs with Docker

Available options:
  -f,--file FILE           Path to the .gitlab-ci.yml file to use
  -j,--job JOBNAME         Job name (as written in .gitlab-ci.yml), e.g
                           release-x86_64-linux-deb9-dwarf
  -u,--user USER           Gitlab user to which the GHC repository belongs
  -p,--project REPONAME    Name of the Gitlab repository/project
  -r,--ref GIT_REF         Git reference to checkout before starting the build
  -h,--help                Show this help text
```

- `--user` defaults to `ghc`
- `--project` defaults to `ghc`
- if no `--ref` is given, the scripts will use master.

# Example

To run the `release-x86_64-linux-deb9-dwarf` job against
`gitlab.haskell.org/ghc/ghc` commit
`64673586da80e10be49258d85fafb2e6129e20a1` using the CI script provided
by `./.gitlab-ci.yml`:

``` sh
$ ghc-repro -f ./.gitlab-ci.yml -j release-x86_64-linux-deb9-dwarf \
            -r 64673586da80e10be49258d85fafb2e6129e20a1
$ chmod +x docker.sh; ./docker.sh
```
